/*
 * Definition de la structure de liste contigue des jours et des prototypes de fonctions associes.
*/

#ifndef DAYS_LIST_H
#define DAYS_LIST_H

#include "agenda.h"

#define TAILLE_MAX 100

/*
 * Element de liste contigue de jours.
*/
typedef struct day_cell_t {
    int year;
    int week;
    int day;
} DayCell;

/*
 * Liste contigue de jours.
*/
typedef struct days_list_t {
    DayCell * first;
    DayCell * last;
} DaysList;

/*
 * Crée une liste de jours.
 * @return une nouvelle liste contigue de jours vide.
*/
DaysList new_days_list();

/*
 * Ajoute un nouveau jour dans une liste contigue de jours.
 * @param list une liste contigue de jours.
 * @param year l'annee.
 * @param week la semaine.
 * @param day le jour.
 * @return si le jour a effectivement ete ajoute.
*/
bool add_days_list(DaysList * list, int year, int week, int day);

/*
 * Affiche une liste contigue de jours.
 * @param file une fichier de sortie ou afficher.
 * @param list une liste contigue de jours.
*/
void print_days_list(FILE * file, DaysList list);

/*
 * Charge une liste contigue de jours a partir d'un agenda et d'un motif.
 * @param agenda un agenda d'entree d'ou charger la liste.
 * @param list une liste contigue de jours a charger.
 * @param pattern un motif de recherche dans les actions de l'agenda.
 * @return true si la liste a bien ete chargee, false s'il y a eu une erreur.
*/
bool load_days_list(Agenda agenda, DaysList * list, char pattern[10]);

/*
 * Vide une liste contigue de jours.
 * @param list une liste contigue de jours.
*/
void clear_days_list(DaysList * list);


#endif