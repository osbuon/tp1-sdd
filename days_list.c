/*
 * Corps des fonctions definies dans days_list.h.
*/

#include <stdlib.h>
#include <string.h>

#include "days_list.h"


DaysList new_days_list() {
    //Une liste vide a ces deux pointeurs qui pointent vers NULL.
    DaysList list;
    list.first = NULL;
    list.last = NULL;
    return list;
}


bool add_days_list(DaysList * list, int year, int week, int day) {
    bool sucess;

    if (list->last - list->first >= TAILLE_MAX) sucess = false; //Si la liste est pleine.
    else {
        //Si la liste est vide, on doit allouer l'espace mémoire contigue.
        if (list->first == NULL)
            list->first = (DayCell *) malloc(sizeof(DayCell) * TAILLE_MAX);

        if (list->first != NULL) { //Si la liste est bien allouee.
            //On repositionne la pointeur de fin.
            if (list->last == NULL) list->last = list->first;
            else list->last++;
            
            list->last->year = year;
            list->last->week = week;
            list->last->day = day;

            sucess = true;
        } else sucess = false;
    }

    return sucess;
}

void print_days_list(FILE * file, DaysList list) {
    if (list.first != NULL) {
        DayCell * cour;
        for (cour = list.first; cour <= list.last; cour++)
            fprintf(file, "%04d %02d %01d\n", cour->year, cour->week, cour->day);
    }
}

bool load_days_list(Agenda agenda, DaysList * list, char pattern[10]) {
    bool fail = false;

    List1Element * cour1 = agenda;

    while (!fail && cour1 != NULL) { //Si on atteint la fin de l'agenda ou s'il y a eu un probleme d'ajout dans la liste.
        List2Element * cour2 = cour1->tasks;

        while (!fail && cour2 != NULL) {
            //On transforme en une chaine de caractere avec un caractere nul a la fin.
            char name[11];
            for (int i = 0; i < 10; i++)
                name[i] = cour2->name[i];
            name[10] = '\0';

            if (strstr(name, pattern) != NULL)
                fail = !add_days_list(list, cour1->year, cour1->week, cour2->day);

            cour2 = cour2->next;
        }

        cour1 = cour1->next;
    }

    return !fail;
}

void clear_days_list(DaysList * list) {
    if (list->first != NULL) {
        free(list->first);
        list->first = NULL;
        list->last = NULL;
    }
}