/*
 * Definition de la structure d'agenda et des prototypes de fonctions associes.
*/

#ifndef AGENDA_H
#define AGENDA_H

#include <stdbool.h>
#include <stdio.h>

struct list2_element_t;


/*
 * Element de liste du premier niveau.
*/
typedef struct list1_element_t {
    int year;
    int week;
    struct list2_element_t * tasks;
    struct list1_element_t * next;
} List1Element;

/*
 * Element de liste du second niveau.
*/
typedef struct list2_element_t {
    int day;
    int hour;
    char name[10];
    struct list2_element_t * next;
} List2Element;

/*
 * Liste du premier niveau.
*/
typedef List1Element * List1;

/*
 * Un autre nom pour List1.
*/
typedef List1 Agenda;

/*
 * Cree un nouvel agenda.
 * @return un nouvel agenda vide.
*/
Agenda new_agenda();

/*
 * Ajoute une nouvelle tache dans un agenda.
 * @param agenda un agenda.
 * @param year l'annee.
 * @param week la semaine.
 * @param day le jour.
 * @param hour l'heure.
 * @param name le nom de la tache sur 10 caracteres.
 * @return si la tache a effectivement ete ajoutee.
*/
bool add_agenda_element(Agenda * agenda, int year, int week, int day, int hour, char name[10]);

/*
 * Supprime des taches dans un agenda.
 * @param agenda un agenda.
 * @param year l'annee.
 * @param week la semaine.
 * @param day le jour.
 * @param hour l'heure.
 * @return le nombre de taches effectivement supprimees.
*/
int remove_agenda_element(Agenda * agenda, int year, int week, int day, int hour);

/*
 * Enregistre un agenda dans un fichier.
 * @param file le fichier de sortie ou enregistrer ou afficher.
 * @param agenda un agenda.
*/
void print_agenda(FILE * file, Agenda agenda);

/*
 * Charge un agenda depuis un fichier.
 * @param file le fichier d'entrer d'ou charger l'agenda.
 * @param agenda un pointeur vers l'agenda à charger.
 * @return true si l'agenda a bien ete chargee, false s'il y a eu une erreur.
*/
bool load_agenda(FILE * file, Agenda * agenda);

/*
 * Vide un agenda.
 * @param agenda un agenda.
*/
void clear_agenda(Agenda * agenda);


#endif