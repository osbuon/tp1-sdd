/*
 * Definition de toutes les fonctions de test.
*/


#include "days_list.h"


/*
 * Tests de la fonction add_agenda_element.
*/
void test_add_agenda_element() {
    Agenda agenda = new_agenda();

    add_agenda_element(&agenda, 2013, 05, 4, 19, "test 1"); //Cas vide.
    add_agenda_element(&agenda, 2013, 05, 4, 06, "test 2");
    add_agenda_element(&agenda, 2066, 28, 2, 16, "test 3"); //Cas general.
    add_agenda_element(&agenda, 2019, 04, 7, 12, "test 4");
    add_agenda_element(&agenda, 2066, 28, 2, 16, "test 5"); //Cas doublon.

    print_agenda(stdout, agenda);
    clear_agenda(&agenda);
    printf("\n");
}

void test_remove_agenda_element(int year, int week, int day, int hour) {
    Agenda agenda = new_agenda();

    add_agenda_element(&agenda, 2013, 05, 4, 19, "test 1");
    add_agenda_element(&agenda, 2013, 05, 4, 06, "test 2");
    add_agenda_element(&agenda, 2066, 28, 2, 16, "test 3");
    add_agenda_element(&agenda, 2019, 04, 7, 12, "test 4");
    add_agenda_element(&agenda, 2066, 28, 2, 16, "test 5");

    remove_agenda_element(&agenda, year, week, day, hour);
    print_agenda(stdout, agenda);
    clear_agenda(&agenda);
    printf("\n");
}

void test_load_agenda(char filename[256]) {
    Agenda agenda = new_agenda();

    FILE * file = fopen(filename, "r");
    if (file) {
        load_agenda(file, &agenda);
        print_agenda(stdout, agenda);
        fclose(file);
        clear_agenda(&agenda);
    }
    printf("\n");
}

void test_load_days_list(char pattern[256]) {
    Agenda agenda = new_agenda();
    DaysList list = new_days_list();

    add_agenda_element(&agenda, 2013,05,4,19,"test 1167");
    add_agenda_element(&agenda, 2013,05,4,06,"test 1273");
    add_agenda_element(&agenda, 2019,04,7,12,"test 1379");
    add_agenda_element(&agenda, 2066,28,2,16,"test 1780");
    add_agenda_element(&agenda, 2066,28,2,16,"test 1594");
    add_agenda_element(&agenda, 2045,45,7,13,"test 1398");
    add_agenda_element(&agenda, 1978,04,7,12,"test 1347");
    add_agenda_element(&agenda, 2027,04,7,12,"test 1392");
   
    load_days_list(agenda, &list, pattern);
    print_days_list(stdout, list);
    clear_agenda(&agenda);
    clear_days_list(&list);
    printf("\n");
}