/*
 * Fichier main.
*/


#include "test.c"


int main(int argc, char ** argv) {
    //Chargement d'un agenda via la ligne de commande.
    Agenda agenda = new_agenda();
    if (argc == 2) {
        FILE * file = fopen(argv[1], "r");
        if (file) {
            load_agenda(file, &agenda);
            fclose(file);
        }
    }
    print_agenda(stdout, agenda);


    //Tests
    test_add_agenda_element();
    
    test_remove_agenda_element(2013, 05, 4, 06); //Cas premiere tache.
    test_remove_agenda_element(2019, 04, 7, 12); //Cas general.
    test_remove_agenda_element(2066, 28, 2, 16); //Cas doublon.

    test_load_agenda("test1.txt"); //Cas general.
    test_load_agenda("test2.txt"); //Cas fichier vide.

    test_load_days_list("SDD"); //Cas d'aucune correpondance du motif dans le nom des taches de l'agenda.
    test_load_days_list("17"); //Cas d'une correspondance.
    test_load_days_list("13"); //Cas de plusieurs correspondances.


    return 0;
}
