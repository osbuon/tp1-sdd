/*
 * Corps des fonctions definies dans agenda.h.
*/

#include <stdlib.h>

#include "agenda.h"


Agenda new_agenda() {
    return NULL; //un agenda vide est un pointeur vers NULL.
}

bool add_agenda_element(Agenda * agenda, int year, int week, int day, int hour, char name[10]) {
    bool sucess = true; //Permet de savoir si les allocations ont reussies.

    List1Element ** ptr1 = agenda; //Pointeur de pointeur vers la semaine.

    //On cherche la semaine ou ajouter la nouvelle tache.
    while (*ptr1 != NULL && ((*ptr1)->year < year || ((*ptr1)->year == year && (*ptr1)->week < week)))
        ptr1 = &(*ptr1)->next;
    
    //Si la semaine n'existe pas, on la cree dans l'agenda.
    if (*ptr1 == NULL || (*ptr1)->year != year || (*ptr1)->week != week) {
        
        List1Element * tmp = *ptr1;
        *ptr1 = malloc(sizeof(List1Element));
        if (*ptr1 != NULL) {
            (*ptr1)->year = year;
            (*ptr1)->week = week;
            (*ptr1)->tasks = NULL;
            (*ptr1)->next = tmp;
        } else sucess = false;
    }

    if (sucess) {
        List2Element ** ptr2 = &(*ptr1)->tasks; //Pointeur de poiteur vers la tache dans la semaine.

        //On cherche l'endroit ou ajouter la nouvelle tache.
        while (*ptr2 != NULL && ((*ptr2)->day < day || ((*ptr2)->day == day && (*ptr2)->hour < hour)))
            ptr2 = &(*ptr2)->next;
        
        //On cree la cellule et on l'insere.
        List2Element * tmp = *ptr2;
        *ptr2 = malloc(sizeof(List2Element));
        if (*ptr2 != NULL) {
            (*ptr2)->day = day;
            (*ptr2)->hour = hour;

            bool end = false;
            for (int i = 0; i < 10; i++) {
                if (name[i] == '\0') end = true;
                else (*ptr2)->name[i] = name[i];
                if (end) (*ptr2)->name[i] = ' ';
            }
            
            (*ptr2)->next = tmp;
        } else { //Si l'allocation a echouee.
            //On libere la semaine si elle est vide.
            if ((*ptr1)->tasks == NULL) {
                List1Element * tmp = *ptr1;
                *ptr1 = (*ptr1)->next;
                free(tmp);
            }
            sucess = false;
        }
    }

    return sucess;
}

int remove_agenda_element(Agenda * agenda, int year, int week, int day, int hour) {
    int n = 0; //Le nombre de taches supprimees.

    List1Element ** ptr1 = agenda; //Pointeur de pointeur vers la semaine.

    //On cherche la semaine ou supprimer les taches.
    while (*ptr1 != NULL && ((*ptr1)->year < year || ((*ptr1)->year == year && (*ptr1)->week < week)))
        ptr1 = &(*ptr1)->next;
    
    if (*ptr1 != NULL && (*ptr1)->year == year && (*ptr1)->week == week) { //Si la semaine a ete trouvee.
        List2Element ** ptr2 = &(*ptr1)->tasks; //Pointeur de poiteur vers une tache dans la semaine.

        //On cherche les taches dans la semaine.
        while (*ptr2 != NULL && ((*ptr2)->day < day || ((*ptr2)->day == day && (*ptr2)->hour <= hour))) {

            if ((*ptr2)->day == day && (*ptr2)->hour == hour) { //Si la tache correspond.
                List2Element * tmp = *ptr2;
                *ptr2 = (*ptr2)->next;
                free(tmp);
                n++;
            } else ptr2 = &(*ptr2)->next;
        }

        //On supprime la semaine de l'agenda si elle ne contient plus de tache.
        if ((*ptr1)->tasks == NULL) {
            List1Element * tmp = *ptr1;
            *ptr1 = (*ptr1)->next;
            free(tmp);
        }
    }

    return n;
}

void print_agenda(FILE * file, Agenda agenda) {
    List1Element * cour1 = agenda;
    while (cour1 != NULL) {
        List2Element * cour2 = cour1->tasks;
        
        while (cour2 != NULL) {
            fprintf(file, "%04d%02d%01d%02d", cour1->year, cour1->week, cour2->day, cour2->hour);
            for (int i = 0; i < 10; i++)
                fprintf(file, "%c", cour2->name[i]);
            fprintf(file, "\n");

            cour2 = cour2->next;
        }

        cour1 = cour1->next;
    }
}

bool load_agenda(FILE * file, Agenda * agenda) {
    char s[256], y[4+1], w[2+1], d[1+1], h[2+1], name[10];
    bool fail = false; //Permet de savoir s'il y a eu une erreur d'allocation.
    
    while (!fail && fgets(s, 256, file) != NULL) {                 
        for (int i = 0; i < 4; i++)
            y[i] = s[i];
        y[4] = '\0';

        for (int i=4; i<6; i++)
            w[i-4] = s[i];
        w[2] = '\0';

        for (int i = 6; i < 7; i++)
            d[i-6] = s[i];
        d[1] = '\0';

        for (int i = 7; i < 9; i++)
            h[i-7] = s[i];
        h[2] = '\0';

        for (int i = 9; i < 19; i++)
            name[i-9] = s[i];

        fail = !add_agenda_element(agenda, atoi(y), atoi(w), atoi(d), atoi(h), name);

        //On libere la memoire s'il y a eu une erreur.
        if (fail) clear_agenda(agenda);
    }

    return !fail;
}

void clear_agenda(Agenda * agenda) {
    List1Element * cour1 = *agenda;

    while (cour1 != NULL) {
        List2Element * cour2 = cour1->tasks;

        while (cour2 != NULL) {
            List2Element * tmp = cour2->next;
            free(cour2);

            cour2 = tmp;
        }
        List1Element * tmp = cour1->next;
        free(cour1);

        cour1 = tmp;
    }

    *agenda = NULL;
}
